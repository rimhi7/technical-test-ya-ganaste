FROM node:16.18.1-alpine3.16 AS build-front
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:16.18.1-alpine3.16
WORKDIR /app
COPY --from=build-front /app/build /app/build
COPY server.js /app/server.js
RUN npm i express
RUN npm i pm2 -g
CMD [ "pm2-runtime", "/app/server.js" ]