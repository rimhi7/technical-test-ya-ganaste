import {useEffect} from "react";
import {EnhancedStore} from "@reduxjs/toolkit";
import axios, {AxiosError, AxiosResponse, InternalAxiosRequestConfig} from "axios";
import {useGeneralString} from "../../strings";

export const useInterceptor = (store:EnhancedStore)=>{
    const {dispatch} = store
    const {FLAG_MASSAGE_SUCCESS, FLAG_MASSAGE_ERROR} = useGeneralString()
    const handleRequestSuccess = (request: InternalAxiosRequestConfig<Response>) => {
        return request;
    };
    const handleRequestError = (error: Error) => {
        console.error(`REQUEST ERROR! => ${error}`);
        return error;
    };

    const handleResponseSuccess = (response: AxiosResponse) => {
        if (response.status === 200) {
            dispatch({
                type:FLAG_MASSAGE_SUCCESS
            })
        }
        return response;
    };
    const handleResponseError = (error: AxiosError) => {
        if (error.code === "ERR_BAD_RESPONSE") {
            dispatch({
                type: FLAG_MASSAGE_ERROR,
            });
        }
        throw error;
    };
    useEffect(() => {
        axios.defaults.baseURL = `${process.env.REACT_APP_API}`;
        axios.defaults.params = {};
        axios.interceptors.request.use(handleRequestSuccess, handleRequestError);
        axios.interceptors.response.use(handleResponseSuccess, handleResponseError);
    }, []);
}