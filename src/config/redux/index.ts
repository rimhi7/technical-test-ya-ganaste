import { persist, store } from "./store";

export const useReduxConfig = () => {
    return {
        store,
        persist,
    };
};
