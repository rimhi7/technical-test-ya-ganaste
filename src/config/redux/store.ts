import { configureStore } from "@reduxjs/toolkit";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import { useReducers } from "../../models";

// eslint-disable-next-line react-hooks/rules-of-hooks
const reducers = useReducers();

const persistReduce = persistReducer(
    {
        key: "root",
        storage: storage,
        whitelist: ["bank"],
    },
    reducers
);

export const store = configureStore({
    reducer: persistReduce,
    preloadedState: {},
    devTools: process.env.NODE_ENV === "development",
    middleware: [thunk],
});

export const persist = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
