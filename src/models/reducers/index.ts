import {useGeneralReducer} from "./general";
import {combineReducers} from "redux";
import {useBankReducer} from "./bank";

export const useReducers = ()=>{
    const general = useGeneralReducer()
    const bank = useBankReducer()
    return combineReducers({
        general,
        bank,
    })
}