import {initialStates} from "../../initialStates";
import {useGeneralString} from "../../../strings";
import {ActionReducerMapBuilder, AnyAction, createReducer} from "@reduxjs/toolkit";

export const useGeneralReducer = () => {
    const {general} = initialStates()
    const {FLAG_MASSAGE_SUCCESS,FLAG_MASSAGE_ERROR} = useGeneralString()
    const builderCallback = (builder:ActionReducerMapBuilder<any>)=>{
        builder
            .addCase(FLAG_MASSAGE_SUCCESS,(state,action:AnyAction)=>{
                return{
                    ...state,
                    message:{
                        ...state.message,
                        success: !state.message.success,
                        message: action.payload
                    }
                }
            })
            .addCase(FLAG_MASSAGE_ERROR,(state,action:AnyAction)=>{
                return{
                    ...state,
                    message:{
                        ...state.message,
                        error: !state.message.error,
                        message: action.payload
                    }
                }
            })
    }
    return createReducer(general,builderCallback)

}