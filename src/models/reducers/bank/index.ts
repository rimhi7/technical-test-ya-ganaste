import {initialStates} from "../../initialStates";
import {useBankString} from "../../../strings";
import {ActionReducerMapBuilder, AnyAction, createReducer} from "@reduxjs/toolkit";

export const useBankReducer = () => {
    const {bank} = initialStates()
    const {GET_BANKS,SEARCH_BANKS} = useBankString()
    const builderCallback = (builder:ActionReducerMapBuilder<any>)=>{
        builder
            .addCase(GET_BANKS,(state,action:AnyAction)=>{
                return{
                    ...state,
                   list:action.payload,
                   original:action.payload
                }
            })
            .addCase(SEARCH_BANKS,(state,action:AnyAction)=>{
                return{
                    ...state,
                    list:action.payload,
                }
            })
    }
    return createReducer(bank,builderCallback)

}