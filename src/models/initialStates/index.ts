import {initialStateBank} from "./bank";
import {initialStateGeneral} from "./general";

export const initialStates = ()=>{
    const general = initialStateGeneral()
    const bank = initialStateBank()
    return {
        general,
        bank
    }
}