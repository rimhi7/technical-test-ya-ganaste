import {Message} from "../../interfaces/general";

export const initialStateGeneral = () => {
    const message:Message = {
        success:false,
        error:false,
        message:''
    }
    return {
        message,
    }
}