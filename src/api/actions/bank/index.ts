import {Dispatch} from "react";
import {AnyAction} from "@reduxjs/toolkit";
import {useBankProviders} from "../../providers";
import {AxiosResponse} from "axios";
import {Bank} from "../../../models/interfaces/bank";
import {useBankString, useGeneralString} from "../../../strings";

export const useBankActions =  ()=> {
    const {getBanks} = useBankProviders()
    const {GET_BANKS} =useBankString()
    // const {FLAG_MASSAGE_SUCCESS,FLAG_MASSAGE_ERROR} = useGeneralString()
    const getBanksAction = ()=> async(dispatch:Dispatch<AnyAction>)=> {
        try {
            const response:AxiosResponse<Bank[]> = await getBanks()
            dispatch({
                type:GET_BANKS,
                payload:response.data
            })
            // dispatch({type:FLAG_MASSAGE_SUCCESS})
        }catch (e) {
            console.log(e)
            // dispatch({type:FLAG_MASSAGE_ERROR})
        }

    }
    return{
        getBanksAction
    }
}