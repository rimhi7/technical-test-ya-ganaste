import axios from "axios";
import {trackPromise} from "react-promise-tracker";

export const useBankProviders = ()=>{
    const getBanks = ()=>{
        const response = axios({
            method:"GET",
            url:'/banks'
        })
        return trackPromise(response)
    }
    return {
        getBanks
    }
}