import React from "react";
import {Bank} from "../../../models/interfaces/bank";
type Props ={
    bank:Bank
}
const Card = ({bank}:Props)=> {
    return (
    <div className="flip-card">
        <div className="flip-card-inner">
            <div className="flip-card-front">
                <img src={bank.url}/>
            </div>
            <div className="flip-card-back">
                <img src={bank.url}/>
                <h1>{bank.bankName}</h1>
                <h3>{bank.description}</h3>
                <h3>{bank.age} años de experiencia</h3>
            </div>
        </div>
    </div>
    )
}
export default Card