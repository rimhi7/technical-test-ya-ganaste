import React,{Suspense} from "react";
import {useBankScreenController} from "../../../controller";
import {Card} from "../../components";
import {map} from 'lodash'
import {CardSkeleton} from "../../skeletons";
export const Bank = ()=>{
    const {bank} = useBankScreenController()
    return (
        <div className="container-card">
                {map(bank.list,(bank)=>
                    <Suspense key={bank.bankName+bank.age} fallback={<CardSkeleton/>}><Card bank={bank} /></Suspense>
                )}
        </div>
    )
}