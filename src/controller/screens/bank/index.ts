import {useBankActions} from "../../../api";
import {useAppDispatch, useAppSelector} from "../../../config/redux/hooks";
import {Dispatch, useEffect} from "react";

export const useBankScreenController = () => {
    const general = useAppSelector((state)=>state.general);
    const bank = useAppSelector((state)=> state.bank)
    const dispatch:Dispatch<any> = useAppDispatch()
    const {getBanksAction} = useBankActions()

    useEffect(()=>{
        if(bank.original.length<1){
            getBanks()
        }
    },[])
    const getBanks = ()=>{
        dispatch(getBanksAction())
    }
    return{
        general,
        bank
    }
}