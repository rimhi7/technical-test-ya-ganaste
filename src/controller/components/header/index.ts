import {Dispatch, useEffect, useState} from "react";
import {useDebaunced} from "../../../helper";
import {useAppDispatch, useAppSelector} from "../../../config/redux/hooks";
import {filter, toLower} from 'lodash'
import {AnyAction} from "@reduxjs/toolkit";
import {useBankString} from "../../../strings";
export const useHeaderComponentController = () =>{
    const [searchText, setSearchText] = useState("");
    const {debouce} = useDebaunced(searchText)
    const {original}= useAppSelector((state)=>state.bank)
    const dispatch:Dispatch<AnyAction> = useAppDispatch()
    const {SEARCH_BANKS} = useBankString()
    useEffect(()=>{
        const banks = filter(original,(bank)=>{
            const value = toLower(debouce)
            if(toLower(bank.description).includes(value) || toLower(bank.bankName).includes(value)){
                return true
            }
            return false
        })
        dispatch({
            type:SEARCH_BANKS,
            payload:banks
        })

    },[debouce])

    return{
        searchText,
        setSearchText
    }
}