export const useGeneralString = () => {
    const FLAG_MASSAGE_SUCCESS = 'FLAG_MASSAGE_SUCCESS'
    const FLAG_MASSAGE_ERROR = 'FLAG_MASSAGE_ERROR'
    return {
        FLAG_MASSAGE_SUCCESS,
        FLAG_MASSAGE_ERROR,
    }
}