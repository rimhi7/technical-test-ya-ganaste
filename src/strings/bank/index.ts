export const useBankString = () => {
    const GET_BANKS = 'GET_BANKS'
    const SEARCH_BANKS = 'SEARCH_BANKS'
    return {
        GET_BANKS,
        SEARCH_BANKS,
    }
}