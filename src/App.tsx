import React from 'react';
import './App.css';
import {Bank} from "./views";
import {Provider} from "react-redux";
import {useInterceptor, useReduxConfig} from "./config";
import {PersistGate} from "redux-persist/integration/react";
import {Header} from "./views/components";
import {createTheme, ThemeProvider} from "@mui/material";

function App(){
    const {store,persist} = useReduxConfig()
    useInterceptor(store)
    const theme = createTheme(
        // {
        //     palette:{
        //         primary:{
        //             main:'#00A3E0'
        //         }
        //     }
        // }
    )
  return (
      <ThemeProvider theme={theme}>
          <Provider store={store} >
              <PersistGate persistor={persist}>
                  <Header/>
                  <Bank/>
              </PersistGate>
          </Provider>
      </ThemeProvider>


  );
}

export default App;
